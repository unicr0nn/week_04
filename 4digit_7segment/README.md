# 4Digit 7segment display

## Esp32 Useable pins:
https://randomnerdtutorials.com/esp32-pinout-reference-gpios/  

## Pins of the display:
![](pics/7seg.jpg)

## Connecting to ESP32
| ESP32 PIN     | 4X7S Disp  PIN |
| ------------- | -------------- |
|        2      |       A        |
|        4      |       B        |
|        5      |       C        |
|       13      |       D        |
|       14      |       E        |
|       15      |       F        |
|       16      |       G        |
|       17      |      DEC       |
|       18      |      D1        |
|       19      |      D2        |
|       21      |      D3        |
|       22      |      D4        |  

## Dont forget to place 100[ohm] resistors between A - DEC and ESP32 pins 
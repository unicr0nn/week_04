#include <Arduino.h>

#define LED_A   2
#define LED_B   4
#define LED_C   5
#define LED_D   13
#define LED_E   14
#define LED_F   15
#define LED_G   16
#define LED_DOT 17

#define DIGIT_1 18
#define DIGIT_2 19
#define DIGIT_3 21
#define DIGIT_4 22

                  //HGFEDCBA
#define DISP_OFF  0b00000000 
#define DISP_0    0b00111111
#define DISP_1    0b00000110
#define DISP_2    0b01011011
#define DISP_3    0b01001111
#define DISP_4    0b01100110
#define DISP_5    0b01101101
#define DISP_6    0b01111101
#define DISP_7    0b00000111
#define DISP_8    0b01111111
#define DISP_9    0b01100111

                     //4321
#define DISP_DIGIT_1 0b0001
#define DISP_DIGIT_2 0b0010
#define DISP_DIGIT_3 0b0100
#define DISP_DIGIT_4 0b1000

const uint8_t digit_pin[]   = {DIGIT_1, DIGIT_2, DIGIT_3, DIGIT_4};
const uint8_t digit_arr[]   = {DISP_DIGIT_1, DISP_DIGIT_2, DISP_DIGIT_3, DISP_DIGIT_4};
const uint8_t display_pin[] = {LED_A, LED_B, LED_C, LED_D, LED_E, LED_F, LED_G, LED_DOT};
const uint8_t display_num[] = {DISP_0, DISP_1, DISP_2, DISP_3 ,DISP_4 ,DISP_5 ,DISP_6 ,DISP_7 ,DISP_8, DISP_9, DISP_OFF};

uint8_t fourDigits[] = {DISP_0, DISP_0, DISP_0, DISP_0};
uint16_t cntr = 0;
uint16_t num = 0;


void setup() {
  for (uint8_t i = 0; i < 7; i++) {
      pinMode(display_pin[i], OUTPUT);
  }

  for (uint8_t i = 0; i < 4; i++) {
      pinMode(digit_pin[i], OUTPUT);
  }
  
  for (uint8_t i = 0; i < 7; i++) {
    digitalWrite(display_pin[i], HIGH);
  }

    for (uint8_t i = 0; i < 4; i++) {
    digitalWrite(digit_pin[i], LOW);
  }

  Serial.begin(9600);
}

void show(uint8_t num, uint8_t disp) { 
  for(uint8_t i = 0; i < 4; i++) {
    if(disp & 1 << i)
      digitalWrite(digit_pin[i], HIGH);
    else
      digitalWrite(digit_pin[i], LOW);
  }

  for(uint8_t i = 0; i < 7; i++) {
    if(num & 1 << i)
      digitalWrite(display_pin[i], LOW);
    else
      digitalWrite(display_pin[i], HIGH);
  }
}

void calc4DigitNumber(uint16_t number){
  uint8_t tousands = number / 1000;
  uint8_t hundreds = (number - tousands * 1000) / 100;
  uint8_t tens     = (number - tousands * 1000 - hundreds * 100) / 10;
  uint8_t ones     = number - tousands * 1000 - hundreds * 100 - tens * 10; 

  fourDigits[0] = display_num[tousands];
  fourDigits[1] = display_num[hundreds];
  fourDigits[2] = display_num[tens];
  fourDigits[3] = display_num[ones];
}

void display4Digits() {
  for(uint8_t i = 0; i < 4; i++) {
    show(fourDigits[i], digit_arr[i]);
    delay(1);
  }
}

void loop() {

  if(cntr >= 10) {
    cntr = 0;
    calc4DigitNumber(num);
    num++;
    if(num >= 9999) {
      num = 0;
    }
  } else {
    cntr++;
  }

  display4Digits();
  
}

#include <Arduino.h>
#include <LiquidCrystal.h>

#define PIN_RS      2
#define PIN_ENABLE  4
#define PIN_D1      5
#define PIN_D2      13
#define PIN_D3      14
#define PIN_D4      15

LiquidCrystal lcd(PIN_RS, PIN_ENABLE, PIN_D1, PIN_D2, PIN_D3, PIN_D4);

byte cat1[8] = 
{
  0b00011,
  0b00100,
  0b01000,
  0b10001,
  0b10010,
  0b10011,
  0b10000,
  0b10000,
};

byte cat2[8] = 
{
  0b11000,
  0b00101,
  0b00101,
  0b11010,
  0b00010,
  0b11110,
  0b00000,
  0b00000,
};

byte cat3[8] = 
{
  0b10000,
  0b01000,
  0b01111,
  0b00000,
  0b00000,
  0b01000,
  0b01000,
  0b00000,
};

byte cat4[8] = 
{
  0b01000,
  0b10100,
  0b10100,
  0b00010,
  0b00010,
  0b01010,
  0b01010,
  0b00010,
};

byte cat5[8] = 
{
  0b01000,
  0b01000,
  0b01000,
  0b01000,
  0b01000,
  0b00100,
  0b00100,
  0b00111,
};

byte cat6[8] = 
{
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b10000,
  0b11111,
  0b11110,
};

byte cat7[8] = 
{
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00001,
  0b11001,
  0b01111,
};

byte cat8[8] = 
{
  0b00010,
  0b00010,
  0b00100,
  0b00100,
  0b00100,
  0b00100,
  0b00100,
  0b11100,
};


void setup() {
  lcd.begin(16,2);

  //Save custom characters to locations
  lcd.createChar(0, cat1);
  lcd.createChar(1, cat2);
  lcd.createChar(2, cat3);
  lcd.createChar(3, cat4);
  lcd.createChar(4, cat5);
  lcd.createChar(5, cat6);
  lcd.createChar(6, cat7);
  lcd.createChar(7, cat8);

  lcd.clear();
}

void showCat(uint8_t position) {
  lcd.clear();
  //top
  lcd.setCursor(position,0);
  lcd.write(byte(0));
  lcd.setCursor(position + 1,0);
  lcd.write(byte(1));
  lcd.setCursor(position + 2,0);
  lcd.write(byte(2));
  lcd.setCursor(position + 3,0);
  lcd.write(byte(3));
  //bot
  lcd.setCursor(position, 1);
  lcd.write(byte(4));
  lcd.setCursor(position + 1,1);
  lcd.write(byte(5));
  lcd.setCursor(position + 2,1);
  lcd.write(byte(6));
  lcd.setCursor(position + 3,1);
  lcd.write(byte(7));
}


void loop() {

  char temp = '!';
  for(uint8_t i = 0; i < 2; i++) {
    for(uint8_t j = 0; j < 16; j++) {
      lcd.setCursor(j,i);
      lcd.write(temp);
      temp++;
      delay(150);
    }
  }
  delay(1000);
  lcd.clear();

  for (uint8_t i = 0; i < 9; i++) {
    showCat(i);
    delay(600);
  }

  lcd.setCursor(12,0);
  lcd.print("MEOW");

  delay(1500);
  lcd.clear();

}
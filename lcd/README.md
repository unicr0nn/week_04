# 16 x 2 Lcd Display

## Esp32 Useable pins:
https://randomnerdtutorials.com/esp32-pinout-reference-gpios/ 

## LCD panel datasheet:
https://www.openhacks.com/uploadsproductos/eone-1602a1.pdf

## Connect LCD and ESP 32
![](pics/lcd.jpg)

## Used Arduino library:
https://github.com/arduino-libraries/LiquidCrystal

## How to install library to PIO IDE:
https://www.youtube.com/watch?v=_pSVzV4PdiA <-- Ultimate Indian Accent, just get used to it.

## Help for making custom characters. Only 8 custom character can be maid this way
![](pics/grid.jpg)